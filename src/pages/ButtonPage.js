import Button from "../components/Button";
import { GiBowTie, GiAmericanFootballBall, GiAndroidMask, GiAnkh, GiArrowsShield } from "react-icons/gi";

export default function ButtonPage() {
    const handleClick = () => {
        console.log('Click!!')
    }
    return (
        <div>
            <div>
                <Button success rounded outline className="mb-5" onClick={handleClick}><GiBowTie /> Click me!</Button>
            </div>
            <div>
                <Button danger outline onMouseEnter={handleClick}><GiAmericanFootballBall /> Buy Now!</Button>
            </div>
            <div>
                <Button warning onMouseLeave={handleClick}><GiAndroidMask />See Deal!</Button>
            </div>
            <div>
                <Button secondary outline><GiAnkh />Hide Ads!</Button>
            </div>
            <div>
                <Button primary rounded><GiArrowsShield />Somethind!</Button>
            </div>
        </div>
    )
}