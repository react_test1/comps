

export default function AccordionPage() {

    const items = [
        {
            id: 'dsfsdf',
            label: 'Can I use React on a project',
            content: 'You can use React on any project you want.'
        },
        {
            id: 'dsfee',
            label: 'Can I use Javascript on a project',
            content: 'You can use Javascript on any project you want.'
        },
        {
            id: 'ytuytu',
            label: 'Can I use CSS on a project',
            content: 'You can use CSS on any project you want.'
        }
    ]

    return (
        <Accordion items={items} />
    )
}