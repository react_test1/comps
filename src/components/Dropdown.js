import { useEffect, useState, useRef } from "react";
import { GoChevronDown } from "react-icons/go";
import Panel from "./Panel";

export default function Dropdown({ options, value, onChange }) {
    const [isOpen, setIsOpen] = useState('false')
    const divEl = useRef()


    useEffect(() => {
        const handler = (event) => {
            if (!divEl.current) {
                return;
            }
            // NOTE: click อยู่ในองค์ประกอบของ div ของเราหรือไม่
            if (!divEl.current.contains(event.target)) {
                setIsOpen(false)
            }
        }
        document.addEventListener('click', handler, true)

        return () => {
            document.addEventListener('click', handler)
        }
    }, [])


    const handleClick = () => {
        setIsOpen(!isOpen)
    }

    window.timeTwo = performance.now()
    const handleOptionClick = (option) => {
        window.timeOne = performance.now()
        // NOTE: ❌ Close Dropdown
        setIsOpen(false)
        // NOTE: what option id the user click on????
        // console.log(option)
        onChange(option)
    }

    const renderedOptions = options.map((option) => {
        return (
            <div className="hover:bg-sky-100 round cursor-pointer p-1" onClick={() => handleOptionClick(option)} key={option.value}>{option.label}</div>
        )
    })

    return (
        <div ref={divEl} className="w-48 relative">
            <Panel className="flex justify-between items-center cursor-pointer" onClick={handleClick}>
                {value?.label || 'Select...'}
                <GoChevronDown className="text-lg" />
            </Panel>
            {isOpen && <Panel className="absolute top-full">{renderedOptions}</Panel>}
        </div>
    )
}